terraform {
  required_version = ">= 0.12"
}

provider "aws" {
  region = var.aws_region
}

# Create a VPC to launch our instances into
resource "aws_vpc" "default" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name        = var.tags.Name
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

# Create an internet gateway to give our subnet access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.default.id

  tags = {
    Name        = var.tags.Name
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.default.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.default.id
}

# Create a subnet to launch our instances into
resource "aws_subnet" "default" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name        = var.tags.Name
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

# Create a subnet to launch our instances into
resource "aws_subnet" "private" {
  vpc_id                  = aws_vpc.default.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = false

  tags = {
    Name        = "${var.tags.Name}-private"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}

resource "aws_security_group" "public" {
  name        = "${var.tags.Name}-public"
  description = "Used in the terraform"
  vpc_id      = aws_vpc.default.id

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "private" {
  name        = "${var.tags.Name}-private"
  description = "Used in the terraform"
  vpc_id      = aws_vpc.default.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
  }
}

resource "aws_key_pair" "auth" {
  key_name   = var.key_name
  public_key = file("${var.public_key_path}/${var.project_name}.pub")
}

data "aws_ami" "darkaster_ami" {
  most_recent = true
  owners      = ["self"]

  filter {
    name   = "name"
    values = ["darkaster-k8s-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

}

resource "aws_instance" "bastion" {
  ami = data.aws_ami.darkaster_ami.id

  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    type = "ssh"
    # The default username for our AMI
    user = "ubuntu"
    host = self.public_ip
    # The connection will use the local SSH agent for authentication.
  }

  tags = {
    Name        = "${var.tags.Name}-bastion"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }

  instance_type = "t2.micro"

  # Lookup the correct AMI based on the region we specified
  # ami = data.aws_ami.darkaster_ami.id

  # The name of our SSH keypair we created above.
  key_name = aws_key_pair.auth.id

  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = [aws_security_group.public.id, aws_security_group.private.id]

  # We're going to launch into the same subnet as our ELB. In a production
  # environment it's more common to have a separate private subnet for
  # backend instances.
  subnet_id = aws_subnet.default.id

  # # We run a remote provisioner on the instance after creating it.
  # # In this case, we just install nginx and start it. By default,
  # # this should be on port 80
  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo apt-get -y update",
  #     "sudo apt-get -y install nginx",
  #     "sudo service nginx start",
  #   ]
  # }
}

resource "aws_instance" "master" {
  ami = data.aws_ami.darkaster_ami.id
  instance_type = "t2.micro"
  key_name = aws_key_pair.auth.id
  subnet_id = aws_subnet.private.id
  vpc_security_group_ids = [aws_security_group.private.id]
  
  tags = {
    Name        = "${var.tags.Name}-master"
    Owner       = var.tags.Owner
    Environment = var.tags.Environment
  }
}
