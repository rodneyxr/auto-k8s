variable "project_name" {
  default = "darkaster-k8s"
}

variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "us-east-1"
}

variable "key_name" {
  description = "Desired name of AWS key pair"
  default = "darkaster-k8s"
}

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.
Ensure this keypair is added to your local SSH agent so provisioners can
connect.
Example: ~/.ssh/terraform.pub
DESCRIPTION
  default = "~/.ssh"
}

variable "aws_ami_name" {
  default = "darkaster-k8s"
}

variable "tags" {
  default = {
    Name        = "rr-dak8s"
    Owner       = "rrodriguez"
    Environment = "rrodriguez"
  }
}