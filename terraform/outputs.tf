resource "local_file" "ansible_inventory" {
  content = templatefile("inventory.tmpl",
    {
      bastion-id = aws_instance.bastion.id,
      bastion-dns = aws_instance.bastion.public_dns,
      bastion-public-ip = aws_instance.bastion.public_ip,
      bastion-private-ip = aws_instance.bastion.private_ip,
      master-id = aws_instance.master.*.id,
      master-dns = aws_instance.master.*.private_dns,
      master-private-ip = aws_instance.master.*.private_ip
    }
  )
  filename = "inventory"
}