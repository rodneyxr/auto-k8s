# auto-k8s

## Install Requirements
* Packer - https://learn.hashicorp.com/packer/getting-started/install
* Terraform - https://learn.hashicorp.com/terraform/getting-started/install.html
* Ansible - https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

## Instructions
```bash
cd packer
packer build k8s-node.json
```